# MicroVue - Application boilerplate using Micronaut and Vue.js 
This project is a very simple boilerplate to get started to build applications using Micronaut and Vue.js.

Comes out of the box with:
- A micronaut application skeleton
- A VueJs Frontend and build scripts
- A Ripple component library example (see https://github.com/dpc-sdp/ripple) 
- A nginx engine serving static files
- A Mysql database to store some data (this is not used yet)

## Backend
The backend is a micronaut setup

## Frontend
The frontend is a single (or multi) page application written in vue. An simple nginx setup is used to serve 
static files and route everything to the index.

This example is created by Vue cli as a clean project. Two Ripple components installed just for demo how to install them.

### Dependencies

Install below dev dependencies in your Vue project to make icon work.

```
npm install sass-resources-loader --save-dev
```

### Webpack configs

Some config required for making things like icon working.

Check [/vue.config.js](/vue.config.js) for more details.

### Using Ripple components

Check [/src/App.vue](/src/App.vue) and [/src/components/HelloRipple.vue](/src/components/HelloRipple.vue) for more details.

### Install Ripple plugin

Optionally install [Ripple plugin]((https://github.com/dpc-sdp/ripple/tree/develop/packages/components/Atoms/Global#rploptions)) if you need to use some options.

We added it in this [/src/main.js](/src/main.js).

### Project setup
```
yarn install
```
or
```
npm install
```

### Compiles and hot-reloads for development
```
yarn serve
```
or
```
npm run serve
```


## Using docker-compose

The easiest way to run the app is to use the script provided:

```
chmod +x run.sh
./run.sh
```

**Visit:** http://localhost/index.html

Alternately, follow the steps below to build the app (and run it) manually.

**1. Build the Hello microservice**

```
cd server
./gradlew clean build shadowJar
```

**2. Build the sample Vue.js app**

```
cd ../website
npm install
npm run build
```

**3. Assemble and run**

```
cd ..
docker-compose up
```

**Visit:** http://localhost/index.html

### Roadmap
- Integrate yarn and node build script into gradle, controlling both node and yarn versions.
- Create a database backed API endpoint
