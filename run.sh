#!/usr/bin/env bash

BASEDIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

cd $BASEDIR/server
./gradlew clean build shadowJar

cd $BASEDIR/website
npm install
npm run build

cd $BASEDIR
docker-compose up
